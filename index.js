'use strict'

const DB = require('./db')
const FileParser = require('./fileParser')
const log = console.log

; (async () => {
  try {
    const dbConnection = new DB()
    const fp = new FileParser(dbConnection)
    await fp.parseFilesIntoDB()
    dbConnection.endConnection()
  } catch (error) {
    log('Error:', error)
  }
})()
