# File Parser Challenge

[![Dependency Status](https://david-dm.org/nitrohorse/file-parser-challenge.svg)](https://david-dm.org/nitrohorse/file-parser-challenge)
[![License](https://img.shields.io/badge/license-GPLv3-yellow.svg)](https://github.com/nitrohorse/mars-rovers-challenge/blob/master/LICENSE)
[![Standard Style](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![Say Thanks!](https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg)](https://saythanks.io/to/nitrohorse)

## Challenge definition

You receive drops of data files and specification files. Write an application
in the language of your choice that will load these files into a database.

## Challenge details

Data files will be dropped in the folder "data/" relative to your application
and specification files will be dropped in the folder "specs/" relative to
your application.

Specification files will have filenames equal to the file type they specify and
extension of ".csv". So "fileformat1.csv" would be the specification for files
of type "fileformat1".

Data files will have filenames equal to their file format type, followed by
an underscore, followed by the drop date and an extension of ".txt". 
For example, "fileformat1_2007-10-01.txt" would be a
data file to be parsed using "specs/fileformat1.csv", which arrived on 10/01/2007.

Format files will be csv formated with columns "column name", "width", and
"datatype". 

* "column name" will be the name of that column in the database table  
* "width" is the number of characters taken up by the column in the data file  
* "datatype" is the SQL data type that should be used to store the value
in the database table.

Data files will be flat text files with rows matching single records for the
database. Rows are formatted as specified by the associated format file.

## Examples

This is an example file pair; other files may vary in structure while still
fitting the structure of the challenge details (above):

specs/testformat1.csv

```text
"column name",width,datatype
name,10,TEXT
valid,1,BOOLEAN
count,3,INTEGER
```

data/testformat1_2015-06-28.txt

```text
Foonyor   1  1
Barzane   0-12
Quuxitude 1103
```

Sample table output: 
```text
| name      | valid | count |
| --------- | ----- | ----- |
| Foonyor   | True  | 1     |
| Barzane   | False | -12   |
| Quuxitude | True  | 103   |
```

## Solution
* Login to MySQL instance: `mysql -u root -p`
* Create new database: `create database test;`
* Set database: `use test;`
* Show all databases: `show databases;`
* Run (steps below): `yarn start`
* Show all tables: `show tables;`
* Show records for table, testformat1: `select * from testformat1;`
```
+-----------+-------+-------+
| name      | valid | count |
+-----------+-------+-------+
| Foonyor   |     1 |     1 |
| Barzane   |     0 |   -12 |
| Quuxitude |     1 |   103 |
+-----------+-------+-------+
```
* Cleanup:
  * `drop table testformat1;`
  * `drop database test;`

## Run
* Install tools
  * [Node.js + npm](https://nodejs.org/en/)
  * [Yarn](https://yarnpkg.com/en/)
* Install dependencies
  * `yarn`
* Start
  * Add your MySQL username and password to `config.js`
  * `yarn start`

### Output
```
Opening DB connection
Parsing spec files...
[ { 'column name': 'name', width: '10', datatype: 'TEXT' },
  { 'column name': 'valid', width: '1', datatype: 'BOOLEAN' },
  { 'column name': 'count', width: '3', datatype: 'INTEGER' } ]
Creating table: create table if not exists testformat1 (name TEXT(10), valid BOOLEAN, count INTEGER(3))
Parsing associated data files...
Bulk inserting: insert into testformat1 (name, valid, count) values ?
values: [ [ 'Foonyor', 1, 1 ],
  [ 'Barzane', 0, -12 ],
  [ 'Quuxitude', 1, 103 ] ]
Done!
Closing DB connection
```