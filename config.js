'use strict'

const config = {
  database: {
    host: 'localhost',
    user: '',
    password: '',
    database: 'test'
  }
}

module.exports = config
