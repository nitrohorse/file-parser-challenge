'use strict'

const csv = require('csvtojson')
const globby = require('globby')
const readline = require('readline')
const fs = require('fs')

const log = console.log

class FileParser {
  constructor (dbConnection) {
    this.dbConnection = dbConnection
  }

  readSpecFile (specFilePath) {
    return csv().fromFile(specFilePath)
  }

  getSpecFileName (specFilePath) {
    return specFilePath.substring(specFilePath.lastIndexOf('/') + 1, specFilePath.indexOf('.csv'))
  }

  getAssociatedDataFilePaths (specFileName, dataFilesPaths) {
    return dataFilesPaths.filter(dataFilePath => {
      if (dataFilePath.includes(specFileName)) {
        return dataFilePath
      }
    })
  }

  castToDataType (columnValue, datatype) {
    switch (datatype) {
      case 'BOOLEAN':
      case 'INTEGER':
        columnValue = Number(columnValue)
        break
      default:
        break
    }
    return columnValue
  }

  readDataFile (spec, dataFilePath) {
    const dataFileInterface = readline.createInterface({
      input: fs.createReadStream(dataFilePath)
    })

    const dataArray = []

    return new Promise(resolve => {
      dataFileInterface.on('line', line => {
        let charIndex = 0
        const values = spec.map(columnDetail => {
          const columnWidth = Number(columnDetail['width'])
          const columnDataType = columnDetail['datatype']

          // read line char by char
          const endIndex = charIndex + columnWidth
          let columnValue = line.substring(charIndex, endIndex)
          charIndex = endIndex

          columnValue = columnValue.trim()

          columnValue = this.castToDataType(columnValue, columnDataType)
          return columnValue
        })
        dataArray.push(values)
      })

      dataFileInterface.on('close', () => {
        resolve(dataArray)
      })
    })
  }

  async parseFilesIntoDB () {
    try {
      const specFilesPaths = await globby('./specs/*.csv')
      const dataFilesPaths = await globby('./data/*.txt')

      log('Parsing spec files...')
      for (let specFilePath of specFilesPaths) {
        const spec = await this.readSpecFile(specFilePath)
        log(spec)

        const tableName = this.getSpecFileName(specFilePath)
        const associatedDataFilePaths = this.getAssociatedDataFilePaths(tableName, dataFilesPaths)
        await this.dbConnection.createTable(tableName, spec)

        log('Parsing associated data files...')
        for (let dataFilePath of associatedDataFilePaths) {
          const values = await this.readDataFile(spec, dataFilePath)
          await this.dbConnection.bulkInsert(tableName, spec, values)
        }
      }
      log('Done!')
    } catch (error) {
      log('Error:', error)
    }
  }
}

module.exports = FileParser
